var express = require('express');
var router = express.Router();

var cacheMap = {};

const DEFAULT_TTL_IN_SECONDS = 30;

router.delete('/', function (req, res) {
    cacheMap = {};
    res.status(200);
    res.send('All messages have been successfully deleted');
})

router.get('/:id', function (req, res, next) {
    const currentTime = new Date();
    if (cacheMap[req.params.id] && ((currentTime - cacheMap[req.params.id].postTime) / 1000 <= cacheMap[req.params.id].document.ttl)) {
        res.status(200)
        res.send(cacheMap[req.params.id].document);
    } else {
        res.status(404)
        res.send('Resource Not Found');
    }
});

router.post('/', function (req, res, next) {
    const document = applyDefaultTTL(req.body);

    cacheMap[req.body.id] = {
        postTime: new Date(),
        document: document
    }
    res.status(200);
    res.send('ok, cached: ' + JSON.stringify(cacheMap[req.body.id].document));
});

function applyDefaultTTL(request) {
    if (!request.ttl) {
        return {
            id: request.id,
            message: request.message,
            ttl: DEFAULT_TTL_IN_SECONDS
        }
    } else {
        return {
            id: request.id,
            message: request.message,
            ttl: request.ttl
        }
    }
}

module.exports = router;