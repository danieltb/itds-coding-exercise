var express = require('express');
var router = express.Router();

var messagesRouter = require('./messages/messages');

router.use('/messages', messagesRouter);

module.exports = router;