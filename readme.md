# RESTFUL CACHING API #

Implementation of a Nodejs webserver which will store JSON requests for a period of time (default 30 seconds).

To run locally run:
- npm install
- npm start

The following endpoints are available:
- POST /messages sending JSON which must contain an id filed which will be used as a key. Optional field of ttl which will specify how long the message will be cached for in seconds.
- GET /messages/<id> used to retrieve the message stored against the key
- DELETE /messages will delete all keys.

This is currently deployed at: https://mysterious-scrubland-31898.herokuapp.com